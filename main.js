// Obtenemos una referencia a la base de datos de firebase
var database = firebase.database();

// VARIABLES GLOBALES
var departamentos = [];		// Lista de todos los Departamentos
var markers = [];					// Lista de todfos los puntos que se muestran en el mapa
var map;									// Mapa

// Inicializa el mapa
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -17.0000000, lng: -65.0000000},
    zoom: 5
  });
}

// Este método se activa automaticamente cuando se inicia la aplicación y cuando se modifica la base de datos,
// esta es la razon por la que no es necesario recargar la página, cuando se modifica la base de datos
database.ref('departamentos/').on('value', function(snapshot) {
  departamentos = snapshot.val();
  updateMarkers();
  console.log("Datos actualizados");
});

// Actualiza todas las marcas
function updateMarkers() {
  clearMarkers();
  for (var i = 0; i < departamentos.length; i++) {
    colocalMarcaCadaCiertoTiempo(departamentos[i], i * 200);
  }
}

// Coloca marcas cada cierto periodo de tiempo
function colocalMarcaCadaCiertoTiempo(departamento, tiempoEspera) {
  var posicion = departamento.posicion;
  window.setTimeout(function() {

    // Información de la marca
    var infowindow = new google.maps.InfoWindow({
      content: '<p>'+departamento.titulo+'</p>'
    });

    // Marca
    var marker = new google.maps.Marker({
      position: {lat: posicion.latitud, lng: posicion.longitud},
      map: map,
      animation: google.maps.Animation.DROP
    });

    // Evento al pasar el mouse sobre la marca
    marker.addListener('mouseover', function() {
      infowindow.open(map, marker);
    });

    // Evento al quitar el mouse de la marca
    marker.addListener('mouseout', function() {
      infowindow.close(map, marker);
    });

    // Adiciona la marca
    markers.push(marker);
  }, tiempoEspera);
}

// Borra todas las marcas
function clearMarkers() {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
  markers = [];
}
